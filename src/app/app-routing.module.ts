import { NgModule, Component, ModuleWithProviders } from "@angular/core";
import {RouterModule,Routes} from '@angular/router';
import { StudentsViewComponent } from './students/view/students.view.component';
import { StudentsAddComponent } from './students/add/students.add.component';
import { StudentsComponent } from './students/list/students.component';
import { FileNotFoundComponent } from './shared/file-not-found/file-not-found.component';

const appRoutes: Routes = [
    {
        path:'',redirectTo: '/detail/1',pathMatch: 'full'
    },
    {path: '**', component: FileNotFoundComponent}
];

@NgModule({
    imports: [
        RouterModule.forRoot(appRoutes)
    ],
    exports: [
        RouterModule
    ]
})
export class AppRoutingModule{}

//export const AppRoutingModule: ModuleWithProviders = RouterModule.forRoot(appRoutes);

