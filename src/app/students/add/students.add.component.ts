import { Component } from '@angular/core';
import Student from '../../entity/student';
import { StudentService } from 'src/app/service/student-service';

@Component({
  selector: 'app-students-add',
  templateUrl: './students.add.component.html',
  styleUrls: ['./students.add.component.css']
})
export class StudentsAddComponent {
  students: Student[];
  constructor(private studentService: StudentService) { }

  upQuantity(student: Student) {
    student.penAmount++;
  }

  downQuantity(student: Student) {
    if (student.penAmount > 0) {
      student.penAmount--;
    }
  }

}
